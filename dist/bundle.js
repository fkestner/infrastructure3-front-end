/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/css/style.scss":
/*!****************************!*\
  !*** ./src/css/style.scss ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./src/js/addartist.js":
/*!*****************************!*\
  !*** ./src/js/addartist.js ***!
  \*****************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
window.addEventListener('DOMContentLoaded', () => {
    const form = document.querySelector('#add-artist-form')

    form.addEventListener('submit', async function(event) {
        event.preventDefault() // Prevent the form from submitting normally

        let name = document.querySelector('#artist-name').value
        let topCountry = document.querySelector('#top-country').value
        let netWorth = document.querySelector('#net-worth').value

        let artistData = {
            name: name,
            topCountry: topCountry,
            netWorth: netWorth
        }

        console.log(artistData);

        // If the data is valid, send the POST request
        let response = await fetch('http://localhost:8080/api/artist', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(artistData)
        })

        if (response.ok) {
            // If the request was successful, redirect to the home page
            const artist = await response.json();
            console.log(artist);
            window.location.href = `http://localhost:8080/artistPage/${artist.id}`
        } else {
            console.log(response);
            // throw new Error(`HTTP error! status: ${response.status}`)
        }
    })
})

/***/ }),

/***/ "./src/js/config.js":
/*!**************************!*\
  !*** ./src/js/config.js ***!
  \**************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
const API_URL = "https://infra3-back-end-usv2awkrea-ew.a.run.app" || 0;

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (API_URL);

/***/ }),

/***/ "./src/js/navbar.js":
/*!**************************!*\
  !*** ./src/js/navbar.js ***!
  \**************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
// take all the navigation items
const navItems = document.querySelectorAll('.nav-link');
// for each navigation link...
navItems.forEach(navItem => {
    // ... add a click listener (something should happen)
    navItem.addEventListener('click', () => {
        // navItem.href is the full url: "http://www..." etc.
        const url = new URL(navItem.href);
        // extract the hashtag out of the full url
        const hash = url.hash;
        // first hide all the sections
        document.querySelectorAll('section').forEach(section => {
            section.style.display = 'none';
        });
        // show the section we need
        document.querySelector(hash).style.display = 'block';
    });
});

/***/ }),

/***/ "./src/js/search.js":
/*!**************************!*\
  !*** ./src/js/search.js ***!
  \**************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _config_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./config.js */ "./src/js/config.js");
document.querySelector('#search').addEventListener('keyup', searchTextChanged);





async function searchTextChanged(e) {
    const resultContainer = document.querySelector('#search-results');
    resultContainer.innerHTML = '<p class="text-muted">Loading...</p>';

    const searchTerm = e.target.value;

    // fetch(`${endpoint}`);

    const response = await fetch(`${_config_js__WEBPACK_IMPORTED_MODULE_0__["default"]}/api/song?searchTerm=${searchTerm}`, {
        headers: {'Accept': 'application/json'}
    });
    const searchResults = await response.json();
    setSearchResults(searchResults, resultContainer);
}

function setSearchResults(songs, container) {
    let html = `<p>Found ${songs.length} Artists</p>`;
    html += '<ul>';
    songs.forEach(song => {
        html += `<li><a href="http://localhost:8080/artistPage/${song.id}">${song.name}</a></li>`;
    });
    html += '</ul>';
    container.innerHTML = html;
}


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _css_style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./css/style.scss */ "./src/css/style.scss");
/* harmony import */ var _js_navbar_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./js/navbar.js */ "./src/js/navbar.js");
/* harmony import */ var _js_search_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./js/search.js */ "./src/js/search.js");
/* harmony import */ var _js_addartist_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./js/addartist.js */ "./src/js/addartist.js");





})();

/******/ })()
;
//# sourceMappingURL=bundle.js.map