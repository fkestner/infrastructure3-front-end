document.querySelector('#search').addEventListener('keyup', searchTextChanged);


import API_URL from './config.js';


async function searchTextChanged(e) {
    const resultContainer = document.querySelector('#search-results');
    resultContainer.innerHTML = '<p class="text-muted">Loading...</p>';

    const searchTerm = e.target.value;

    // fetch(`${endpoint}`);

    const response = await fetch(`${API_URL}/api/song?searchTerm=${searchTerm}`, {
        headers: {'Accept': 'application/json'}
    });
    const searchResults = await response.json();
    setSearchResults(searchResults, resultContainer);
}

function setSearchResults(songs, container) {
    let html = `<p>Found ${songs.length} Artists</p>`;
    html += '<ul>';
    songs.forEach(song => {
        html += `<li><a href="http://localhost:8080/artistPage/${song.id}">${song.name}</a></li>`;
    });
    html += '</ul>';
    container.innerHTML = html;
}
