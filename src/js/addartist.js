window.addEventListener('DOMContentLoaded', () => {
    const form = document.querySelector('#add-artist-form')

    form.addEventListener('submit', async function(event) {
        event.preventDefault() // Prevent the form from submitting normally

        let name = document.querySelector('#artist-name').value
        let topCountry = document.querySelector('#top-country').value
        let netWorth = document.querySelector('#net-worth').value

        let artistData = {
            name: name,
            topCountry: topCountry,
            netWorth: netWorth
        }

        console.log(artistData);

        // If the data is valid, send the POST request
        let response = await fetch('http://localhost:8080/api/artist', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            body: JSON.stringify(artistData)
        })

        if (response.ok) {
            // If the request was successful, redirect to the home page
            const artist = await response.json();
            console.log(artist);
            window.location.href = `http://localhost:8080/artistPage/${artist.id}`
        } else {
            console.log(response);
            // throw new Error(`HTTP error! status: ${response.status}`)
        }
    })
})