const API_URL = process.env.API_URL || 'https://default-backend-url';

export default API_URL;