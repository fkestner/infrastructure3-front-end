// take all the navigation items
const navItems = document.querySelectorAll('.nav-link');
// for each navigation link...
navItems.forEach(navItem => {
    // ... add a click listener (something should happen)
    navItem.addEventListener('click', () => {
        // navItem.href is the full url: "http://www..." etc.
        const url = new URL(navItem.href);
        // extract the hashtag out of the full url
        const hash = url.hash;
        // first hide all the sections
        document.querySelectorAll('section').forEach(section => {
            section.style.display = 'none';
        });
        // show the section we need
        document.querySelector(hash).style.display = 'block';
    });
});